Role Name
=========

Configuring NTP or CHRONY

role installs ntp or chrony as time service depending on freeipa client is installed on host.
Role is working on Debian baseed or RHEL based systems (Debian, Ubuntu, Centos...)

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

    - hosts: all
      roles:
         - ntp

License
-------

Internal use only
